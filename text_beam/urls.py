from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views 


urlpatterns = [
    # Examples:
    # url(r'^$', 'text_beam.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # /admin
    url(r'^admin/', include(admin.site.urls)),

    # /messages
    url(r'^sms/', include('sms.urls', namespace='sms')),

    # /accounts/login
    url(r'^accounts/login/$',  auth_views.login, name='login'),

    # /accounts/signup
    # url(r'^accounts/signup/$', auth_views.signup, name='signup')
]
