from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

# Create your models here.
class Configuration(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, null=True)
    # user configuration data.
    appstore_uri_ios = models.URLField(null=True, blank=True)
    appstore_uri_android = models.URLField(null=True, blank=True)
    website_uri = models.URLField(null=True, blank=False)
    # call to action message sent in the sms.
    message_cta = models.CharField(max_length=128, null=True, blank=True) 

    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)



    def __unicode__(self):
        return self.message_cta[0:30] + '...'
