from django.conf.urls import url
from django.views.generic import TemplateView
from sms import views


urlpatterns = [
  # sms
  url(r'^$', views.index, name='index'),

  # ex: sms/send/15551234567
  url(r'^send/(?P<number>\d{0,11})/$', views.send, name='send'),

  # ex: sms/thanks
  url(r'^thanks/$', TemplateView.as_view(template_name='sms/thanks.html'), name='thanks'),
]
