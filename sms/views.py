# Django imports
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, get_user_model

# external library imports
from twilio.rest import TwilioRestClient
import json

# app imports
from .forms import SignupForm, SigninForm
from .models import Configuration


APP_NAME = 'fit-stitch'
APP_URL = 'https://itunes.apple.com/app/id711835190'

def index(request):
  return HttpResponse('test')



def send(request, number):
     # # validations.
    if number is None:
        return HttpResponse(json.dumps(error='need a number'), 
                            content_type="application/json")


    # Find these values at https://twilio.com/user/account
    account_sid = "ACe4aaabb88cba7820640ef1f1d845e58b"
    auth_token = "b524b897c60899114e0b0c13cfb0f4e3"
    client = TwilioRestClient(account_sid, auth_token)
    US_CA_COUNTRY_CODE = "+1"

    curr_user = get_user_model().get(username='ddevaz')
    user_msg = curr_user.configuration.message_cta
    app_uri = curr_user.configuration.appstore_uri_ios
    #app_name = curr_user.configuration.app_name
    body_text = user_msg + " \n%s" % (app_uri)

    #create and send the message.
    message = client.messages.create(to=number, 
                                  from_="+17786547317",
                                   body=body_text)

    #respond to the post request.
    data_resp = json.dumps({'status': message.status, 
                            'to': message.to, 
                            'body': message.body,  
                            'price': message.price})

    return HttpResponse(data_resp, content_type="application/json")
