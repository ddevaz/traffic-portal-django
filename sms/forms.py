from django import forms

class SignupForm(forms.Form):
    name = forms.CharField(max_length=64)
    email = forms.EmailField()
    password = forms.CharField(max_length=64, min_length=6)


class SigninForm(forms.Form):
    username = forms.CharField(max_length=64)
    password = forms.CharField(max_length=64, min_length=6)